#!/bin/bash

DL_URL="https://ww1.microchip.com/downloads/en/DeviceDoc/xc32-v4.10-linux-installer.run"
SHA256="7a937d92d74ded7ac7e5e3591bc1fb06cb8ad8a602679a5d7f8063f45c2ee8d7"

# download
wget --content-disposition -nv -- $DL_URL
INSTALLER=$(find . -type f -name "xc32-*-linux-installer.run")

FILENAME=`basename $INSTALLER`
XC32_VERSION=${FILENAME#*-}
XC32_VERSION=${XC32_VERSION%%-*}

echo $XC32_VERSION

# check SHA256 sum
if $(echo "$SHA256  $INSTALLER" | sha256sum -cs -); then
    echo "SHA256 sums matched"
else
    echo "SHA256 sums did NOT match! Aborting"
    exit 1
fi

# build docker image
docker build \
    --pull \
    --tag "$CI_REGISTRY_IMAGE:latest" \
    --tag "$CI_REGISTRY_IMAGE:$XC32_VERSION" \
    --build-arg xc32_installer=$FILENAME \
    --build-arg xc32_version=$XC32_VERSION \
    .

# push
docker push "$CI_REGISTRY_IMAGE:latest" 
docker push "$CI_REGISTRY_IMAGE:$XC32_VERSION" 
