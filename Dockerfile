FROM debian:bullseye

ARG xc32_installer
ARG xc32_version

# prevent blocking input from tzdata
ENV DEBIAN_FRONTEND=noninteractive

# install
COPY $xc32_installer /tmp
RUN chmod u+x /tmp/$xc32_installer
RUN /tmp/$xc32_installer --mode unattended --netservername localhost
RUN rm -f /tmp/$xc32_installer

# add build tools to path
ENV PATH=/opt/microchip/xc32/$xc32_version/bin:${PATH}
